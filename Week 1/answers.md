# Answers for the assignments of week 1

## Basisopdrachten
### Assignment 1

a. Verklaar de term Grote-O (Big-Oh)
```
De term Big-Oh staat voor een theoretische maatstaf voor de uitvoering van een algoritme.
Deze is meestal gebaseerd op de tijd of geheugen dat nodig is voor het algoritme.
```

b. Waarvoor gebruik je de Grote-O notatie?
```
Om aan te tonen hoe efficiënt een algoritme is.
```

c. Wat betekende de termen slechtste, beste en gemiddelde geval als we het hebben over de Grote-O (Big-Oh) notatie?
```
Slechtste geval:  Maximaal uitgevoerde operaties.
Beste Geval: Minimaal uitgevoerde operaties.
Gemiddelde Geval: aantal uitgevoerde operaties gebaseerd op een x aantal uitvoeringen van het programma.
```

d. Wat is de orde van grootte (Grote-O) in het slechtste, beste en het gemiddelde geval van de onderstaande code:
```
int i, j, tijdelijk;     
for (j = 0; j < invoer.length; j++) {         
  for (i = 1; i < invoer.length - j; i++) {             
    if (invoer[i-1] > invoer[i]) {
      tijdelijk = invoer[i];
      invoer[i] = invoer[i-1];
      invoer[i-1] = tijdelijk;
    }
  }
}
```
```
Slechtste geval: O(n^2)
Beste geval: O(n)
Gemiddelde geval: O(n^2)
```
### Assignment 2
a. Leg uit wat de term in-place sorteren inhoud.
```
De term in-place sorting houdt in dat de input variabele dat als input dient ook wordt gebruikt als output variabele.
```

b. Leg uit wat een pivot is.
```
Een pivot is een vaste index wat het algoritme gebruikt als vast punt om een conditie te controleren
```

c.  Leg uit hoe je de getallen 1, 8, 7, 3, 2, 5, 6, 4 sorteert met behulp van het quicksort algoritme.
```
Als eerste stap kies je een pivot. We selecteren het hoogste index, getal 4.
We zetten de "Wall" links van de eerste index.
Nu controleren we elke index rechts van de Wall of deze groter of kleiner is dan de pivot (4).
Als de value van de index kleiner is dan zetten we deze links van de wall en gaan we naar de volgende index.
Is de value van de index groter dan doen we hier niets mee en gaan we naar de volgende index.
Als we alles gehad hebben zetten we de pivot op de plek van de wall en schuiven we de wall 1 plek op naar rechts en kiezen een nieuwe pivot.

Na de eerste doorloop hebben we: [1, 3, 2] [4] [8,7,5,6]

De linker subarray is groter dan 1 en zal dus ook gesorteerd moeten worden.
Na 1 doorloop hebben we bij deze: [1] [2] [3] en is dus gesorteerd.

Tweede subarray dient ook gesorteerd te worden aangezien deze groter is dan 1.
Na 1 doorloop hebben we bij deze: [ 5 ] [6] [ 8, 7]
Linker subarray is niet groter dan 1 en hoeft dus niet gesorteerd te worden.
Rechter subarray is groter dan 1 en na 1 doorloop van sorteren is: [7] [8]

Nu zetten we alles weer achter elkaar en krijgen we [1] [2] [3] [4] [5] [6] [7] [8].
of wel [1, 2, 3, 4, 5, 6, 7, 8]
```

## Kennisopdrachten
### Opdracht 4
Implementeer een Quicksort algoritme door de onderstaande methode te gebruiken in Quicksort.java
```
private void quicksort(int low, int high) {
  //Schrijf hier je eigen quicksort implementatie
}
```
Antwoord zie: `code/Quicksort.java`


### Opdracht 5
Herschrijf de code in Quicksort.java zo dat je niet alleen arrays van int’s kan sorteren, maar ook van andere datatypen zoals Strings. Dit kan je doen door gebruik te maken van zogenaamde Generics. Op de onderstaande website kan je hier meer over vinden.
  - http://www.tutorialspoint.com/java/java_generics.htm

Antwoord zie: `code/QuicksortGenerics.java`

### Opdracht 6
Schrijf minimaal 3 unit tests die verschillende scenarios van de bij opdracht 1 geïmplementeerde quicksort algoritme testen. Op de onderstaande websites kan je hier meer over vinden.
  - http://www.tutorialspoint.com/junit/junit_test_framework.htm

Antwoord zie: `code/QuicksortTest.java` en `code/QuicksortGenericsTest.java` 
