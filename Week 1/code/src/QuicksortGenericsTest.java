import junit.framework.TestCase;
import static org.junit.Assert.*;

/**
 * Created by mdbaz on 09-09-2016.
 */
public class QuicksortGenericsTest extends TestCase{
    protected Integer[] integerArray;
    protected Integer[] integerArrayExpectedResult;
    protected String[] stringArray;
    protected String[] stringArrayExpectedResult;
    protected Boolean[] booleanArray;
    protected Boolean[] booleanArrayExpectedResult;
    protected Integer[] switchValueArray;
    protected Integer[] switchValueArrayExpectedResult;

    public void setUp() {
        integerArray = new Integer[] {8, 4, 6, 7 ,1, 3, 2, 9, 0, 5};
        integerArrayExpectedResult = new Integer[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        stringArray = new String[] {"x", "a", "u", "s", "b", "w", "f", "k"};
        stringArrayExpectedResult = new String[] {"a", "b", "f", "k", "s", "u", "w", "x"};

        booleanArray = new Boolean[] {true, false, true, false, false, true};
        booleanArrayExpectedResult = new Boolean[] {false, false, false, true, true, true};


        switchValueArray = new Integer[] {0, 5, 4};
        switchValueArrayExpectedResult = new Integer[] {4, 5, 0};
    }

    public void testQuicksortGenericsInteger() {
        QuicksortGenerics quicksort = new QuicksortGenerics<>(this.integerArray);
        quicksort.start();
        assertArrayEquals(integerArrayExpectedResult, quicksort.array);
    }

    public void testQuicksortGenericsString() {
        QuicksortGenerics quicksort = new QuicksortGenerics<>(this.stringArray);
        quicksort.start();
        assertArrayEquals(stringArrayExpectedResult, quicksort.array);
    }

    public void testQuicksortGenericsBoolean() {
        QuicksortGenerics quicksort = new QuicksortGenerics<>(this.booleanArray);
        quicksort.start();
        assertArrayEquals(booleanArray, quicksort.array);
    }

    public void testQuicksortSwitchValues() {
        QuicksortGenerics quicksort = new QuicksortGenerics<>(switchValueArray);
        quicksort.switchValues(0, 2);
        assertArrayEquals(switchValueArrayExpectedResult, quicksort.array);
    }
}
