import java.util.Random;

/**
 * Created by mdbaz on 08-09-2016.
 */
public class main {

    public static void main(String args[]) {
        Quicksort qs = new Quicksort(100);
        System.out.print("Start: ");
        qs.printArray();
        qs.start();
        System.out.print("End: ");
        qs.printArray();

        Random rand = new Random();
        Integer[] numArray = new Integer[1+rand.nextInt(50)];
        for(int i = 0; i < numArray.length; i++) {
            numArray[i] = rand.nextInt(10000);
        }

        System.out.println("\n\n QuicksortGenerics<Integer>");
        QuicksortGenerics quicksortGenerics = new QuicksortGenerics<>(numArray);
        System.out.print("Start: ");
        quicksortGenerics.printArray();
        quicksortGenerics.start();
        System.out.print("End: ");
        quicksortGenerics.printArray();
    }


}
