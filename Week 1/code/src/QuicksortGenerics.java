import sun.management.Util;

import java.util.Random;

/**
 * Created by mdbaz on 08-09-2016.
 */

public class QuicksortGenerics<T extends Comparable> {
    T[] array;

    public QuicksortGenerics(T[] inputArray) {
        this.array = inputArray;
    }

    public void start() {
        quicksort(0, this.array.length-1);
    }

    public void quicksort(int low, int high) {
        int indexLow = low;
        int indexHigh = high;

        T pivot = array[indexLow + (indexHigh - indexLow)/2 ];

        while(indexLow <= indexHigh) {
            while(array[indexLow].compareTo(pivot) < 0) {
                indexLow++;
            }
            while(array[indexHigh].compareTo(pivot) > 0) {
                indexHigh--;
            }
            if( indexLow <= indexHigh) {
                switchValues(indexLow, indexHigh);
                indexLow++;
                indexHigh--;
            }
        }
        if(low < indexHigh) {
            quicksort(low, indexHigh);
        }
        if(indexLow < high) {
            quicksort(indexLow, high);
        }
    }

    public void switchValues( int indexA, int indexB) {
        T temp = array[indexA];
        array[indexA] = array[indexB];
        array[indexB] = temp;
    }

    public void printArray() {
        for(int i = 0; i < array.length; i++) {
            System.out.print(array[i]+ ", ");
        }
        System.out.print("\n");
    }
}
