/**
 * Created by mdbaz on 09-09-2016.
 */
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        QuicksortTest.class, QuicksortGenericsTest.class
})

public class TestSuite {
}
