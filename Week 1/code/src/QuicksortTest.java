/**
 * Created by mdbaz on 09-09-2016.
 */

import junit.framework.TestCase;
import static org.junit.Assert.*;

public class QuicksortTest extends TestCase {
    protected int[] array;
    protected int[] arrayExpectedResult;
    protected int[] switchValueArray;
    protected int[] switchValueArrayExpectedResult;

    public void setUp() {
        array = new int[] {8, 4, 6, 7 ,1, 3, 2, 9, 0, 5};
        arrayExpectedResult = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        switchValueArray = new int[] {0, 5, 4};
        switchValueArrayExpectedResult = new int[] {4, 5, 0};
    }

    public void testQuicksortWithSetArray() {
        Quicksort quicksort = new Quicksort(this.array);
        quicksort.start();
        assertArrayEquals(arrayExpectedResult, quicksort.array);
    }

    public void testQuicksortConstructorWithSize() {
        int size = 10;
        Quicksort quicksort = new Quicksort(size);
        assertEquals(size, quicksort.array.length);
    }

    public void testQuicksortSwitchValues() {
        Quicksort quicksort = new Quicksort(switchValueArray);
        quicksort.switchValues(0, 2);
        assertArrayEquals(switchValueArrayExpectedResult, quicksort.array);
    }
}
