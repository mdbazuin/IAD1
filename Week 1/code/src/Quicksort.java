/**
 * Created by mdbaz on 08-09-2016.
 */

import java.util.Random;

public class Quicksort {
    int[] array;

    public Quicksort(int size) {
        Random rand = new Random();
        this.array = new int[size];
        for(int i = 0; i < array.length; i++) {
            this.array[i] = rand.nextInt(size*size);
        }
    }

    public Quicksort(int[] array) {
        this.array = array;
    }

    public void start() {
        quicksort(0, this.array.length-1);
    }

    public void quicksort(int low, int high) {
        int indexLow = low;
        int indexHigh = high;

        int pivot = array[indexLow + (indexHigh - indexLow)/2 ];

        while(indexLow <= indexHigh) {
            while(array[indexLow] < pivot) {
                indexLow++;
            }
            while(array[indexHigh] > pivot) {
                indexHigh--;
            }
            if( indexLow <= indexHigh) {
                switchValues(indexLow, indexHigh);
                indexLow++;
                indexHigh--;
            }
        }
        if(low < indexHigh) {
            quicksort(low, indexHigh);
        }
        if(indexLow < high) {
            quicksort(indexLow, high);
        }
    }

    public void switchValues( int indexA, int indexB) {
        int temp = array[indexA];
        array[indexA] = array[indexB];
        array[indexB] = temp;
    }

    public void printArray() {
        for(int i = 0; i < array.length; i++) {
            System.out.print(array[i]+ ", ");
        }
        System.out.print("\n");
    }
}